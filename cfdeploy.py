import boto3, os


# RELEASE_APP1=os.environ['bamboo_app1_release']
# RELEASE_APP2=os.environ['bamboo_app2_release']
# DD_KEY=os.environ['bamboo_datadog_api_key_password']
# RELEASE_WEB=os.environ['bamboo_deploy_release']
# AWS_DEFAULT_REGION=os.environ['aws_region']

role_arn = 'arn:aws:iam::692371712833:role/bamboo'


# this is using sessions and a defined profile. We dont need session!
# leaving it here for future reference
# def get_role_credentials( role_arn ):
#     "return creds from arn"
#     print role_arn
#     #sts_client = boto3.Session(profile_name='mmdev').client('sts') use this to use a specific profile
#     #sts_client = boto3.Session.client('sts')
#     sts_client = boto3.client('sts')
#     assumedRoleObject = sts_client.assume_role(
#         RoleArn=role_arn,
#         RoleSessionName="AssumeRoleSession1"
#     )
#     credentials = assumedRoleObject['Credentials']
#     # we can also write this as: credentials = sts_client.assume_role(RoleArn='arn:aws:iam::692371712833:role/bamboo',RoleSessionName="AssumeRoleSession1")['Credentials']
#     return credentials

def assume_role( role_arn ):
    sts_client = boto3.client('sts')
    temp_cred = sts_client.assume_role(
        RoleArn=role_arn,
        RoleSessionName="bamboosession"
    )
    return boto3.session.Session(
        region_name="eu-west-1",
        aws_access_key_id=temp_cred.get("Credentials").get("AccessKeyId"),
        aws_secret_access_key=temp_cred.get("Credentials").get("SecretAccessKey"),
        aws_session_token=temp_cred.get("Credentials").get("SessionToken"),
    )



def wait_for_stack():
    pass

def stack_exists(stack_name, client):
    response = client.list_stacks(
        StackStatusFilter=[
            'CREATE_IN_PROGRESS',
            'CREATE_FAILED',
            'CREATE_COMPLETE',
            'ROLLBACK_IN_PROGRESS',
            'ROLLBACK_FAILED',
            'ROLLBACK_COMPLETE',
            'DELETE_IN_PROGRESS',
            'DELETE_FAILED',
            'DELETE_COMPLETE',
            'UPDATE_IN_PROGRESS',
            'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
            'UPDATE_COMPLETE',
            'UPDATE_ROLLBACK_IN_PROGRESS',
            'UPDATE_ROLLBACK_FAILED',
            'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS',
            'UPDATE_ROLLBACK_COMPLETE',
            'REVIEW_IN_PROGRESS',
        ]
    )
    for stack in response["StackSummaries"]:
        if stack["StackName"] == stack_name:
            return True
    return False

def get_output_value( stack,key ):
    creds = get_role_credentials('arn:aws:iam::692371712833:role/bamboo')
    aws_access_key_id=creds['AccessKeyId']
    cf_client = boto3.client(
        'cloudformation',
        region_name='eu-west-1',
        aws_access_key_id=ACCESS_KEY,
        aws_secret_access_key=SECRET_KEY,
        aws_session_token=SESSION_TOKEN,
    )

    response = cf_client.describe_stacks(
        StackName=stack,
    )

    return value


# paginator = client.get_paginator('list_stacks')
#    response_iterator = paginator.paginate(
#        StackStatusFilter=stack_states
#    )
#    for stack in response_iterator:
#        print(stack['StackSummaries'][0]['StackName'])


def get_identity():
    sts_client = boto3.Session.client('sts')
    identity = sts_client.get_caller_identity()
    return identity


print stack_exists('network', assume_role(role_arn).client('cloudformation')) #(using another role in another account)
print stack_exists('network', boto3.client('cloudformation')) #using the current account (default session)

sts_client = boto3.client('sts')
print sts_client.get_caller_identity()
print assume_role(role_arn=role_arn).client('sts').get_caller_identity()


#   bash_cmd = "/usr/local/bin/aws cloudformation list-stacks --region eu-west-1 --profile beersaccount"

